﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PizzaDelivery.Models;
using PizzaDelivery.DAL;

namespace PizzaDelivery.Controllers
{
    public class ShoppingCartController : Controller
    {
        PizzaDeliveryContext db = new PizzaDeliveryContext();

        // GET: ShoppingCart
        public ActionResult Index()
        {
            if (Session["cart"] == null)
            {
                return View("Error");
            }
            else
            {
                return View("Cart");
            }
        }

        private int isExisting(int id)
        {
            List<Item> cart = (List<Item>)Session["cart"];
            for (int i = 0; i < cart.Count; i++)
                if (cart[i].Pizza.PizzaID == id)
                    return i;
            return -1;
        }

        public ActionResult Delete(int id)
        {
            int index = isExisting(id);
            List<Item> cart = (List<Item>)Session["cart"];
            cart.RemoveAt(index);
            Session["cart"] = cart;

            return View("Cart");
        }

        public ActionResult Delete1(int id)
        {
            int index = isExisting(id);
            List<Item> cart = (List<Item>)Session["cart"];
            cart.RemoveAt(index);
            Session["cart"] = cart;

            return RedirectToAction("Create", "Orders");
        }

        public ActionResult AddToCart(int id)
        {
            if (Session["cart"] == null)
            {
                List<Item> cart = new List<Item>();
                cart.Add(new Item(db.Pizzas.Find(id), 1));
                Session["cart"] = cart;
            }
            else
            {
                List<Item> cart = (List<Item>)Session["cart"];
                int index = isExisting(id);
                if (index == -1)
                    cart.Add(new Item(db.Pizzas.Find(id), 1));
                else
                    cart[index].Quantity++;
                Session["cart"] = cart;
            }
            return View("Cart");
        }
    }
}