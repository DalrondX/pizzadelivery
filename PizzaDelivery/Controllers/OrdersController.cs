﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PizzaDelivery.DAL;
using PizzaDelivery.Models;
using System.Text;

namespace PizzaDelivery.Controllers
{
    public class OrdersController : Controller
    {
        private PizzaDeliveryContext db = new PizzaDeliveryContext();

        // GET: Orders
        public ActionResult Index()
        {
            return View(db.Orders.ToList());
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrderID,DateOfOrder,FirstName,LastName,Email,Contact,PriceSum,NumberOfPizzas,Address,ZipCode,City")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(order);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderID,DateOfOrder,FirstName,LastName,Email,Contact,PriceSum,NumberOfPizzas,Address,ZipCode,City")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Checkout(Order objOrder)
        {
            TryValidateModel(objOrder);
            List<Order> listOrder = new List<Order>();
            listOrder.Add(objOrder);
            TempData["form"] = listOrder;
            TempData.Keep("form");
            if (ModelState.IsValid)
                return View("Checkout");
            else
                return View("Create");
        }

        public ActionResult Save()
        {
            List<Order> listOrder = new List<Order>();
            List<Item> listItem = new List<Item>();
            listOrder = (List<Order>)TempData["form"];
            listItem = (List<Item>)Session["cart"];
            string pom = "";
            var PriceSum = 0;

            foreach (var item in listOrder)
            {
                pom += String.Format($"Objednávka přijata: {DateTime.Now}\r\nJméno a příjmení: {item.FirstName} {item.LastName}\r\nEmail: {item.Email}\r\nKontakt: {item.Contact}\r\nAdresa doručení: {item.Address}, {item.City}, {item.ZipCode}\r\n\r\nPizza                              Cena           Množství         Mezisoučet\r\n-----------------------------------------------------------------------------\r\n");

                foreach (var item1 in listItem)
                {
                    var pizzaPrice = item1.Pizza.Price * item1.Quantity;
                    PriceSum += pizzaPrice;
                    pom += String.Format($"{item1.Pizza.PizzaName,-35}{item1.Pizza.Price + ",- Kč",-15}{item1.Quantity,-17}{pizzaPrice},- Kč\r\n");
                }
            }
            pom += String.Format($"-----------------------------------------------------------------------------\r\n                                                              Sum: {PriceSum},- Kč");
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "Objednavka.txt", pom);

            return View("Thanks");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
