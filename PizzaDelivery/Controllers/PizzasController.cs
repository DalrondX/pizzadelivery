﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PizzaDelivery.DAL;
using PizzaDelivery.Models;

namespace PizzaDelivery.Controllers
{
    public class PizzasController : Controller
    {
        private PizzaDeliveryContext db = new PizzaDeliveryContext();

        // GET: Pizzas
        public ActionResult Index()
        {
            var pizza = db.Pizzas.Include(b => b.PizzasToIngredients).ToList();
            return View(pizza);
        }

        // GET: Pizzas/Create
        public ActionResult Create()
        {
            var MyViewModel = new PizzasViewModel
            {
                Ingredients = db.Ingredients.Select(c => new CheckBoxViewModel
                {
                    Id = c.IngredientID,
                    IngredientName = c.IngredientName,
                    Checked = false
                }).ToList()
            };

            return View(MyViewModel);
        }

        // POST: Pizzas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PizzasViewModel MyViewModel)
        {
            if (ModelState.IsValid)
            {
                var pizza = new Pizza
                {
                    PizzaName = MyViewModel.PizzaName,
                    Price = MyViewModel.Price,
                    PizzasToIngredients = new List<PizzaToIngredient>()
                };

                foreach (var item in MyViewModel.Ingredients.Where(c => c.Checked))
                {
                    var ingredient = new Ingredient { IngredientID = item.Id };
                    db.Ingredients.Attach(ingredient);

                    var pizzaToIngredient = new PizzaToIngredient
                    {
                        Ingredient = ingredient
                    };

                    pizza.PizzasToIngredients.Add(pizzaToIngredient);
                }

                db.Pizzas.Add(pizza);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(MyViewModel);
        }

        // GET: Pizzas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pizza pizza = db.Pizzas.Find(id);
            if (pizza == null)
            {
                return HttpNotFound();
            }
            var Results = from i in db.Ingredients
                          select new
                          {
                              i.IngredientID,
                              i.IngredientName,
                              Checked = ((from io in db.PizzasToIngredients
                                          where (io.PizzaID == id) & (io.IngredientID == i.IngredientID)
                                          select io).Count() > 0)
                          };

            var MyViewModel = new PizzasViewModel();

            MyViewModel.PizzaID = id.Value;
            MyViewModel.PizzaName = pizza.PizzaName;
            MyViewModel.Price = pizza.Price;

            var MyCheckBoxList = new List<CheckBoxViewModel>();

            foreach (var item in Results)
            {
                MyCheckBoxList.Add(new CheckBoxViewModel { Id = item.IngredientID, IngredientName = item.IngredientName, Checked = item.Checked });
            }
            MyViewModel.Ingredients = MyCheckBoxList;

            return View(MyViewModel);
        }

        // POST: Pizzas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PizzasViewModel pizza)
        {
            if (ModelState.IsValid)
            {
                var MyPizza = db.Pizzas.Find(pizza.PizzaID);
                MyPizza.PizzaName = pizza.PizzaName;
                MyPizza.Price = pizza.Price;

                foreach (var item in db.PizzasToIngredients)
                {
                    if (item.PizzaID == pizza.PizzaID)
                    {
                        db.Entry(item).State = EntityState.Deleted;
                    }

                }

                foreach (var item in pizza.Ingredients)
                {
                    if (item.Checked)
                    {
                        db.PizzasToIngredients.Add(new PizzaToIngredient() { PizzaID = pizza.PizzaID, IngredientID = item.Id });
                    }

                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pizza);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
