﻿using System;
using PizzaDelivery.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PizzaDelivery.DAL
{
    public class PizzaDeliveryContext : DbContext
    {
        public PizzaDeliveryContext() : base("PizzaDeliveryContext")
        {
        }

        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Pizza> Pizzas { get; set; }
        public DbSet<PizzaToIngredient> PizzasToIngredients { get; set; }
        public DbSet<OrderToPizza> OrdersToPizzas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}