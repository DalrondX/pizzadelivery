﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PizzaDelivery.Models
{
    public class PizzasViewModel
    {
        public int PizzaID { get; set; }

        [DisplayName("Pizza")]
        [Required(ErrorMessage = "The name of the pizza is required")]
        [StringLength(50, ErrorMessage = "The name of the pizza can be a maximum of 50 characters")]
        public string PizzaName { get; set; }

        [Required(ErrorMessage = "The price of the pizza is required")]
        [Range(0, 500, ErrorMessage = "The price of the pizza can be in a range of 0 - 500")]
        [DataType(DataType.Currency)]
        public int Price { get; set; }
        public List<CheckBoxViewModel> Ingredients { get; set; }
    }
}