﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PizzaDelivery.Models
{
    public class Ingredient
    {
        [Key]
        public int IngredientID { get; set; }

        [DisplayName("Ingredient")]
        [Required(ErrorMessage = "The name of the ingredient is required")]
        [StringLength(50, ErrorMessage = "The name of the ingredient can be a maximum of 50 characters")]
        public string IngredientName { get; set; }
        public virtual ICollection<PizzaToIngredient> PizzasToIngredients { get; set; }
    }
}