﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PizzaDelivery.Models
{
    public class Order
    {
        [Key]
        public int OrderID { get; set; }

        public DateTime DateOfOrder { get; set; }

        [DisplayName("First name")]
        [Required(ErrorMessage = "First name is required")]
        [StringLength(50, ErrorMessage = "First name can be a maximum of 50 characters")]
        public string FirstName { get; set; }

        [DisplayName("Last name")]
        [Required(ErrorMessage = "Last name is required")]
        [StringLength(50, ErrorMessage = "Last name can be a maximum of 50 characters")]
        public string LastName { get; set; }

        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Invalid email format")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Contact is required")]
        [StringLength(20, ErrorMessage = "Contact can be a maximum of 20 characters")]
        [DataType(DataType.PhoneNumber)]
        public string Contact { get; set; }
        public int PriceSum { get; set; }
        public int NumberOfPizzas { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [StringLength(50, ErrorMessage = "Address can be a maximum of 50 characters")]
        public string Address { get; set; }

        [Required(ErrorMessage = "ZipCode is required")]
        [StringLength(10, ErrorMessage = "ZipCode can be a maximum of 10 characters")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "City is required")]
        [StringLength(50, ErrorMessage = "City can be a maximum of 50 characters")]
        public string City { get; set; }

        public virtual ICollection<OrderToPizza> OrdersToPizzas { get; set; }
    }
}