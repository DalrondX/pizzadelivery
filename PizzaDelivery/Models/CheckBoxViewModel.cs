﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PizzaDelivery.Models
{
    public class CheckBoxViewModel
    {
        public int Id { get; set; }
        public string IngredientName { get; set; }
        public bool Checked { get; set; }
    }
}