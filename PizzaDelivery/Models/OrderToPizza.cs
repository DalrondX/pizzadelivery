﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PizzaDelivery.Models
{
    public class OrderToPizza
    {
        public int OrderToPizzaID { get; set; }
        public int OrderID { get; set; }
        public int PizzaID { get; set; }

        public virtual Pizza Pizza { get; set; }
        public virtual Order Order { get; set; }
    }
}