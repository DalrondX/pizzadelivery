﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PizzaDelivery.Models
{
    public class PizzaToIngredient
    {
        public int PizzaToIngredientID { get; set; }
        public int PizzaID { get; set; }
        public int IngredientID { get; set; }

        public virtual Pizza Pizza { get; set; }
        public virtual Ingredient Ingredient { get; set; }
    }
}