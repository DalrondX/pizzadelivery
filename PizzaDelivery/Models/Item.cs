﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PizzaDelivery.Models
{
    public class Item
    {
        Pizza pizza = new Pizza();
        int quantity;

        public Pizza Pizza { get; set; }
        public int Quantity { get; set; }

        public Item() { }
        public Item(Pizza pizza, int quantity)
        {
            Pizza = pizza;
            Quantity = quantity;
        }
    }
}