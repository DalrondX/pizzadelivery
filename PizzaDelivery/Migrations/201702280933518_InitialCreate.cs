namespace PizzaDelivery.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ingredient",
                c => new
                    {
                        IngredientID = c.Int(nullable: false, identity: true),
                        IngredientName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.IngredientID);
            
            CreateTable(
                "dbo.PizzaToIngredient",
                c => new
                    {
                        PizzaToIngredientID = c.Int(nullable: false, identity: true),
                        PizzaID = c.Int(nullable: false),
                        IngredientID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PizzaToIngredientID)
                .ForeignKey("dbo.Ingredient", t => t.IngredientID, cascadeDelete: true)
                .ForeignKey("dbo.Pizza", t => t.PizzaID, cascadeDelete: true)
                .Index(t => t.PizzaID)
                .Index(t => t.IngredientID);
            
            CreateTable(
                "dbo.Pizza",
                c => new
                    {
                        PizzaID = c.Int(nullable: false, identity: true),
                        PizzaName = c.String(nullable: false, maxLength: 50),
                        Price = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PizzaID);
            
            CreateTable(
                "dbo.OrderToPizza",
                c => new
                    {
                        OrderToPizzaID = c.Int(nullable: false, identity: true),
                        OrderID = c.Int(nullable: false),
                        PizzaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderToPizzaID)
                .ForeignKey("dbo.Order", t => t.OrderID, cascadeDelete: true)
                .ForeignKey("dbo.Pizza", t => t.PizzaID, cascadeDelete: true)
                .Index(t => t.OrderID)
                .Index(t => t.PizzaID);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        DateOfOrder = c.DateTime(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        Email = c.String(),
                        Contact = c.String(nullable: false, maxLength: 20),
                        PriceSum = c.Int(nullable: false),
                        NumberOfPizzas = c.Int(nullable: false),
                        Address = c.String(nullable: false, maxLength: 50),
                        ZipCode = c.String(nullable: false, maxLength: 10),
                        City = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.OrderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PizzaToIngredient", "PizzaID", "dbo.Pizza");
            DropForeignKey("dbo.OrderToPizza", "PizzaID", "dbo.Pizza");
            DropForeignKey("dbo.OrderToPizza", "OrderID", "dbo.Order");
            DropForeignKey("dbo.PizzaToIngredient", "IngredientID", "dbo.Ingredient");
            DropIndex("dbo.OrderToPizza", new[] { "PizzaID" });
            DropIndex("dbo.OrderToPizza", new[] { "OrderID" });
            DropIndex("dbo.PizzaToIngredient", new[] { "IngredientID" });
            DropIndex("dbo.PizzaToIngredient", new[] { "PizzaID" });
            DropTable("dbo.Order");
            DropTable("dbo.OrderToPizza");
            DropTable("dbo.Pizza");
            DropTable("dbo.PizzaToIngredient");
            DropTable("dbo.Ingredient");
        }
    }
}
