namespace PizzaDelivery.Migrations
{
    using DAL;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PizzaDeliveryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PizzaDeliveryContext context)
        {
            //var ingredients = new List<Ingredient>
            //{
            //new Ingredient{Name="Klob�sa"},
            //new Ingredient{Name="�unka"},
            //new Ingredient{Name="Paprika"},
            //new Ingredient{Name="Eidam"},
            //new Ingredient{Name="Anananas"},
            //new Ingredient{Name="Kuku�ice"},
            //new Ingredient{Name="Ement�l"},
            //new Ingredient{Name="Niva"},
            //new Ingredient{Name="�ampi�ny"},
            //new Ingredient{Name="Anglick� slanina"},
            //new Ingredient{Name="Ku�ec� maso"},
            //new Ingredient{Name="Vep�ov� maso"}
            //};

            //ingredients.ForEach(s => context.Ingredients.Add(s));
            //context.SaveChanges();

            //var pizzas = new List<Pizza>
            //{
            //    new Pizza{
            //        Name ="Hawai",
            //        Price =150,
            //        Ingredients = new List<Ingredient>()
            //        {
            //            new Ingredient()
            //            {
            //                IngredientID=1,
            //                Name="Salam"
            //            },
            //            new Ingredient()
            //            {
            //                IngredientID=2,
            //                Name="Syr"
            //            },
            //            new Ingredient()
            //            {
            //                IngredientID=3,
            //                Name="Anananas"
            //            },
            //            new Ingredient()
            //            {
            //                IngredientID=4,
            //                Name="Kukurice"
            //            }
            //        },
            //        Orders = new List<Order>()
            //        {
            //            new Order()
            //            {
            //                DateOfOrder = DateTime.Now,
            //                FirstName = "Ond�ej",
            //                LastName = "Lhotsk�",
            //                PriceSum = 150,
            //                NumberOfPizzas = 1,
            //                Address = "Okru�n� 251",
            //                ZipCode = "747 14",
            //                City = "Markvartovice"
            //            }
            //        }
            //    },
            //    new Pizza{
            //        Name ="Sunkova",
            //        Price =300,
            //        Ingredients = new List<Ingredient>()
            //        {
            //            new Ingredient()
            //            {
            //                IngredientID=1,
            //                Name="Salam"
            //            },
            //            new Ingredient()
            //            {
            //                IngredientID=2,
            //                Name="Syr"
            //            },
            //            new Ingredient()
            //            {
            //                IngredientID=5,
            //                Name="Sunka"
            //            }
            //        },
            //        Orders = new List<Order>()
            //        {
            //            new Order()
            //            {
            //                DateOfOrder = DateTime.Now,
            //                FirstName = "Vojt�ch",
            //                LastName = "Lhotsk�",
            //                PriceSum = 600,
            //                NumberOfPizzas = 2,
            //                Address = "Okru�n� 185",
            //                ZipCode = "747 14",
            //                City = "Markvartovice"
            //            }
            //        }
            //    }


            //};
            //pizzas.ForEach(s => context.Pizzas.Add(s));
            //context.SaveChanges();


            //var pizzas = new List<Pizza>
            //{
            //    new Pizza { Name="Hawai", Price=150 },
            //    new Pizza { Name="�unkov�", Price=120 },
            //    new Pizza { Name="�ampionov�", Price=300 },
            //    new Pizza { Name="Tvar��kov�", Price=249 },
            //    new Pizza { Name="T�i druhy s�ra", Price=200 },
            //    new Pizza { Name="Pizza s ku�ec�m masem", Price=300 },
            //    new Pizza { Name="Pizza s vep�ov�m masem", Price=300 }
            //};

            //pizzas.ForEach(s => context.Pizzas.Add(s));
            //context.SaveChanges();

            //var orders = new List<Order>
            //{
            //new Order{DateOfOrder=DateTime.Now,FirstName="Ondrej",LastName="Lhotsky",PriceSum=300,NumberOfPizzas=2,Address="Okru�n� 251",City="Markvartovice",ZipCode="747 14"},
            //new Order{DateOfOrder=DateTime.Now,FirstName="Vojta",LastName="Lhotsky",PriceSum=850,NumberOfPizzas=3,Address="Okru�n� 251",City="Markvartovice",ZipCode="747 14"},
            //new Order{DateOfOrder=DateTime.Now,FirstName="Marek",LastName="Lhotsky",PriceSum=400,NumberOfPizzas=2,Address="Okru�n� 251",City="Markvartovice",ZipCode="747 14"}
            //};

            //orders.ForEach(s => context.Orders.Add(s));
            //context.SaveChanges();

            //Pizza pizza1 = new Pizza()
            //{
            //    Name = "Hawai",
            //    Price = 150,
            //    Ingredients = new List<Ingredient>()
            //    {
            //        new Ingredient()
            //        {
            //            IngredientID=1,
            //            Name="Salam"
            //        },
            //        new Ingredient()
            //        {
            //            IngredientID=4,
            //            Name="Syr"
            //        },
            //        new Ingredient()
            //        {
            //            IngredientID=5,
            //            Name="Anananas"
            //        },
            //        new Ingredient()
            //        {
            //            IngredientID=6,
            //            Name="Kukurice"
            //        }
            //    }
            //};

            //Pizza pizza2 = new Pizza()
            //{
            //    Name = "Sunkova",
            //    Price = 400,
            //    Ingredients = new List<Ingredient>()
            //    {
            //        new Ingredient()
            //        {
            //            IngredientID=2,
            //            Name="Sunka"
            //        },
            //        new Ingredient()
            //        {
            //            IngredientID=3,
            //            Name="Paprika"
            //        },
            //        new Ingredient()
            //        {
            //            IngredientID=4,
            //            Name="Syr"
            //        },
            //        new Ingredient()
            //        {
            //            IngredientID=6,
            //            Name="Kukurice"
            //        }

            //    }
            //};

            //context.Pizzas.Add(pizza1);
            //context.Pizzas.Add(pizza2);
            //base.Seed(context);


            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
