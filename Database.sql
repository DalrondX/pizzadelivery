﻿USE [PizzaDelivery]
GO
ALTER DATABASE PizzaDelivery COLLATE CZECH_CS_AS
SET IDENTITY_INSERT [dbo].[Ingredient] ON
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (1, N'Klobása')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (2, N'Šunka')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (3, N'Paprika')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (4, N'Eidam')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (5, N'Anananas')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (6, N'Kukuřice')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (7, N'Ementál')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (8, N'Niva')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (9, N'Žampióny')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (10, N'Anglická slanina')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (11, N'Kuřecí maso')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (12, N'Vepřové maso')
INSERT [dbo].[Ingredient] ([IngredientID], [IngredientName]) VALUES (13, N'Tvarůžky')
SET IDENTITY_INSERT [dbo].[Ingredient] OFF
SET IDENTITY_INSERT [dbo].[Pizza] ON
INSERT [dbo].[Pizza] ([PizzaID], [PizzaName], [Price]) VALUES (1, N'Hawai', 150)
INSERT [dbo].[Pizza] ([PizzaID], [PizzaName], [Price]) VALUES (2, N'Šunková', 120)
INSERT [dbo].[Pizza] ([PizzaID], [PizzaName], [Price]) VALUES (3, N'Žampionová', 300)
INSERT [dbo].[Pizza] ([PizzaID], [PizzaName], [Price]) VALUES (4, N'Tvarůžková', 249)
INSERT [dbo].[Pizza] ([PizzaID], [PizzaName], [Price]) VALUES (5, N'Tři druhy sýra', 200)
INSERT [dbo].[Pizza] ([PizzaID], [PizzaName], [Price]) VALUES (6, N'Pizza s kuřecím masem', 300)
INSERT [dbo].[Pizza] ([PizzaID], [PizzaName], [Price]) VALUES (7, N'Pizza s vepřovým masem', 300)
SET IDENTITY_INSERT [dbo].[Pizza] OFF
SET IDENTITY_INSERT [dbo].[PizzaToIngredient] ON
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (1, 1, 2)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (2, 1, 4)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (3, 1, 5)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (4, 1, 6)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (5, 2, 4)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (6, 2, 5)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (7, 3, 9)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (8, 3, 4)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (9, 3, 3)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (10, 4, 13)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (11, 4, 2)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (12, 4, 6)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (13, 5, 4)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (14, 5, 7)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (15, 5, 8)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (16, 6, 11)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (17, 6, 10)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (18, 6, 7)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (19, 6, 1)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (20, 7, 4)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (21, 7, 1)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (22, 7, 12)
INSERT [dbo].[PizzaToIngredient] ([PizzaToIngredientID], [PizzaID], [IngredientID]) VALUES (23, 7, 10)
SET IDENTITY_INSERT [dbo].[PizzaToIngredient] OFF